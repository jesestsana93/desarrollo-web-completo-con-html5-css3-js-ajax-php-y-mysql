Curso de Udemy "Desarrollo Web Completo con HTML5, CSS3, JS AJAX PHP y MySQL" por Juan Pablo De la torre Valdez
Duración: 54 horas

Recursos: 
 Símbolos HTML:
 * https://dev.w3.org/html5/html-author/charref
 * https://www.w3schools.com/html/html_entities.asp
 * normalize
 * Box sizing de Paul Irish: https://www.paulirish.com/2012/box-sizing-border-box-ftw/
 * Generador de gradiente de color: https://webgradients.com/
 * Generador de sombra con CSS: https://www.cssmatic.com/
 * Soporte para propiedades CSS: https://caniuse.com/#feat=css-grid
 * https://app.netlify.com/ para subir los sitios a un dominio gratis
 * https://html5boilerplate.com/ es util para arrancar un proyecto de forma consistente
 * https://sweetalert2.github.io/
 * https://heroicons.com/
 * https://tablericons.com/
 PLUGINS jQuery:
 + jquery.animateNumber: http://aishek.github.io/jquery-animateNumber/
 + The final countdown: http://hilios.github.io/jQuery.countdown/
 + Lettering: http://letteringjs.com/
 + Lightbox: https://lokeshdhakar.com/projects/lightbox2/
 + También esta colorbox y fancybox como opciones alternativas a lightbox

Notas:
Por defecto, el font-weight que ponen los navegadores a los encabezados y párrafos es el siguiente:
p{
 	font-weight: 300px;
}
h1{
	font-weight: 700px;
}