/*Tabla 1*/
CREATE TABLE servicios(
	id INT(11) NOT NULL AUTO_INCREMENT,
	nombre VARCHAR(60) NOT NULL,
	precio DECIMAL(6,2) NOT NULL,
	PRIMARY KEY(id)
);

CREATE DATABASE appsalon;
SHOW DATABASES;

DESCRIBE servicios; /*veo la estructura de una tabla*/

/*================================================================
						CRUD
==================================================================*/
/*CREATE - crear los registros*/
INSERT INTO servicios (nombre, precio) VALUES ("Corte de cabello de adulto", 80);
INSERT INTO servicios (nombre, precio) VALUES ("Corte de cabello de niño", 60);
INSERT INTO servicios (nombre, precio) VALUES 
		("Peinado mujer", 80),
		("Peinado hombre", 60);

/*READ - Listar los registros, verlos*/
SELECT * FROM servicios;
SELECT nombre FROM servicios;
SELECT nombre, precio FROM servicios;
SELECT id, nombre, precio FROM servicios ORDER BY precio;
SELECT id, nombre, precio FROM servicios ORDER BY precio DESC;
SELECT id, nombre, precio FROM servicios LIMIT 2;
SELECT id, nombre, precio FROM servicios ORDER BY precio DESC LIMIT 2; 
SELECT id, nombre, precio FROM servicios WHERE id = 3;

/*UPDATE - actualizar un registro*/
UPDATE servicios SET precio = 70 WHERE id = 2;
UPDATE servicios SET nombre = "Corte de cabello de niño actualizado" WHERE id = 2;
UPDATE servicios SET nombre = "Corte de cabello de adulto actualizado", precio = 120 WHERE id = 2;

/*DELETE - eliminar registros*/
DELETE FROM servicios WHERE id = 1;

/*MODIFICAR LA BASE DE DATOS*/
ALTER TABLE servicios ADD descripcion VARCHAR(100) NOT NULL;
ALTER TABLE servicios CHANGE descripcion nuevonombre VARCHAR(11) NOT NULL;
ALTER TABLE servicios DROP COLUMN descripcion;

/*Eliminar tablas*/
DROP TABLE servicios;

CREATE TABLE reservaciones (
	id INT(11) NOT NULL AUTO_INCREMENT,
	nombre VARCHAR(60) NOT NULL,
	apellido VARCHAR(60) NOT NULL,
	hora TIME DEFAULT NULL,
	fecha DATE DEFAULT NULL,
	servicios VARCHAR(255) NOT NULL,
	PRIMARY KEY(id)
);

/*=====================================================================
						OPERACIONES EN SQL
=======================================================================*/
/*Seleccionar servicios por precio*/
SELECT * FROM servicios WHERE precio > 90;
SELECT * FROM servicios WHERE precio >= 80;
SELECT * FROM servicios WHERE precio = 90;
SELECT * FROM servicios WHERE precio < 80;
SELECT * FROM servicios WHERE precio BETWEEN 100 AND 200;

/*Funciones agregadoras*/
SELECT COUNT(id), fecha FROM reservaciones GROUP BY fecha ORDER BY COUNT(id) DESC;
SELECT SUM(precio) AS totalServicios FROM servicios;
SELECT MIN(precio) AS precioMenor FROM servicios;
SELECT MAX(precio) AS precioMayor FROM servicios;

/*Buscar informacion en la base (Buscador web)*/
SELECT * FROM servicios WHERE nombre LIKE 'Corte%';
SELECT * FROM servicios WHERE nombre LIKE 'Lavado%';
SELECT * FROM servicios WHERE nombre LIKE '%Cabello';
SELECT * FROM servicios WHERE nombre LIKE '%Cabello%';

/*Unir dos columnas - Busquedas de productos especificos en tiendas*/
SELECT * FROM reservaciones WHERE id IN(1,3);
SELECT * FROM reservaciones WHERE fecha = "2021-06-28";
SELECT * FROM reservaciones WHERE fecha = "2021-06-28" AND id=1;
SELECT * FROM reservaciones WHERE fecha = "2021-06-28" AND id=1 AND nombre = "Juan";

/*Concatenar*/
SELECT CONCAT(nombre, ' ', apellido) AS nombreCompleto FROM reservaciones;
SELECT * FROM reservaciones WHERE CONCAT(nombre, " ", apellido) LIKE '%Ana Preciado%';

SELECT hora, fecha CONCAT(nombre, " ", apellido) AS 'Nombre completo' 
FROM reservaciones 
WHERE CONCAT(nombre, " ", apellido) 
LIKE '%Ana Preciado%';


/*=======================================================================
					NORMALIZACION
=========================================================================*/
DROP TABLE reservaciones;
/*Tabla 2*/
CREATE TABLE clientes(
	id INT(11) NOT NULL AUTO_INCREMENT,
	nombre VARCHAR(60) NOT NULL,
	apellido VARCHAR(60) NOT NULL,
	telefono VARCHAR(10) NOT NULL,
	email VARCHAR(30) NOT NULL UNIQUE,
	PRIMARY KEY (id)
);

INSERT INTO clientes (nombre, apellido, telefono, email) VALUES ("Jesus", "Sanchez", 512421024, "correo@correo.com");
INSERT INTO clientes (nombre, apellido, telefono, email) VALUES ("Esteban", "Alcantara", 123456789, "correo2@correo.com");

/*Tabla 3*/
CREATE TABLE citas (
	id INT(11) NOT NULL AUTO_INCREMENT,
	fecha DATE NOT NULL,
	hora TIME NOT NULL,
	clienteId INT(11) NOT NULL,
	PRIMARY KEY (id),
	KEY clienteId (clienteId),
	CONSTRAINT cliente_FK
	FOREIGN KEY (clienteId)
	REFERENCES clientes (id)
);

SELECT * FROM citas INNER JOIN clientes ON clientes.id = citas.clienteId;
SELECT * FROM citas LEFT JOIN clientes ON clientes.id = citas.clienteId;
SELECT * FROM citas RIGHT JOIN clientes ON clientes.id = citas.clienteId;

/*Tabla pivote 4*/
CREATE TABLE citasServicios(
	id INT(11) AUTO_INCREMENT,
	citaId INT(11) NOT NULL,
	servicioId INT(11) NOT NULL,
	PRIMARY KEY (id),
	KEY citaId (citaId),
	CONSTRAINT citas_FK
	FOREIGN KEY (citaId)
	REFERENCES citas (id),
	KEY servicioId (servicioId),
	CONSTRAINT servicios_FK
	FOREIGN KEY(servicioId)
	REFERENCES servicios (id)
);

INSERT INTO citasServicios (citaId, servicioId) VALUES (2, 8);

SELECT * FROM citasServicios 
	LEFT JOIN citas ON citas.id = citasServicios.citaId
	LEFT JOIN servicios on servicios.id = citasServicios.servicioId;

INSERT INTO citasServicios (citaId, servicioId) VALUES (2, 3);
INSERT INTO citasServicios (citaId, servicioId) VALUES (2, 4);

SELECT * FROM citasServicios 
	LEFT JOIN citas ON citas.id = citasServicios.citaId
	LEFT JOIN servicios ON servicios.id = citasServicios.servicioId;

SELECT * FROM citasServicios
	LEFT JOIN citas ON citas.id = citasServicios.citaId
	LEFT JOIN clientes ON citas.clienteId = clientes.id
	LEFT JOIN servicios ON servicios.id = citasServicios.servicioId;