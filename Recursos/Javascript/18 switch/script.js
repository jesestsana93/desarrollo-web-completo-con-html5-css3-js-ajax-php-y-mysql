//Switch statement

const metodoPago = 'efectivo';

switch(metodoPago){
	case 'efectivo':
		console.log(`Pagaste con ${metodoPago}`);
		break;
	case 'cheque':
		console.log(`Pagaste con ${metodoPago} revisaremos que tenga fondos`);
		break;
	case 'tarjeta':
		console.log(`Pagaste con ${metodoPago} espere un momento...`);
		break;
	case 'otro':
		hola();
		break;
	default:
		console.log('Aún no has pagado o metodo de pago no válido');
		break;
}

function hola(){
	console.log('Hola amigos');
}