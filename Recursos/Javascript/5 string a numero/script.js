let numero1 = 50,
	numero2 = "10",
	numero3 = "tres",
	numero4 = 10.20;

console.log(numero1 + numero2);
console.log(Number(numero2) + numero1);
console.log(parseInt(numero4) + numero1);
console.log(parseFloat(numero4) + numero1);

let dato;
dato = Number("20");//20
dato = Number("20.20")//20
dato = Number(true);//1
dato = Number(false);//0

dato = parseInt("100");
dato = parseFloat("100.20");

//To Fixed
let numero5 = "10120180.1021082812";
let numero6 = 118398138913.13081893;
console.log(Number(numero5).toFixed(5));
console.log(numero6.toFixed(5));

console.log(dato);
