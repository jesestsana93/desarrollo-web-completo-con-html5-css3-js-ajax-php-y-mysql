//metodos de propiedad: cuando una funcion se coloca dentro de un objeto se dice que es un metodo
const musica = {
	reproducir: function(cancion){
		console.log('Reproduciendo la cancion: ' + cancion);
	},
	pausar: function(){
		console.log('Paused...');
	}
}
//los metodos tambien pueden ir por fuera
musica.borrar = function(id){
	console.log('Borrando la funcion con el ID: ' + id);
}

musica.reproducir('Hookas and sheridans');
musica.pausar();
musica.reproducir('Bubalu');
musica.borrar(121);
