//un arreglo
const pendientes = ['Tarea', 'Comer', 'Proyecto', 'Estudiar Javascript'];

//un arreglo con objetos
const carrito = [
	{id: 1, producto: 'Libro'},
	{id: 2, producto: 'Camisa'},
	{id: 3, producto: 'Disco'}
];

//un Objeto
let automovil = {
	modelo: 'Camaro',
	motor: 6.0,
	anio: 1967,
	marca: 'Chevrolet'
}

/*for(let i=0; i<pendientes.length;i++){
	console.log(pendientes[i]);
}*/
console.log('***********ITERANDO DE UN ARREGLO*******')
for(pendiente of pendientes){
	console.log(pendiente);
}
console.log('*******ITERANDO EN UN ARREGLO CON OBJETOS******')
for(producto of carrito){
	console.log(producto.producto);
}
//para  un objeto es diferente
console.log('********ITERANDO DE UN OBJETO*******')
for(info of Object.values(automovil)){
	console.log(info);
}

console.log('*******USANDO FOR EACH******')
pendientes.forEach(function(tarea){
	console.log(tarea);
});
/*
pendientes.forEach(tarea =>{
	console.log(tarea);
});
*/
console.log('/***FOR EACH ARREGLO****/')
let nuevoArreglo = pendientes.forEach(tarea =>{});
console.log(nuevoArreglo);

console.log('/***MAP ARREGLO****/')
let nuevoArreglo2 = pendientes.map(tarea =>{
	return tarea;
});
console.log(nuevoArreglo2);

console.log('******FOR EACH VALUES******')
Object.values(automovil).forEach(tarea => {
	console.log(tarea);
})

console.log('******FOR EACH KEYS******')
Object.keys(automovil).forEach(tarea => {
	console.log(tarea);
})

//El map crea una copia de un arreglo o de lo que esta iterando, for each no
console.log('/******MAP*************/');
pendientes.map(tarea =>{
	console.log(tarea);
});