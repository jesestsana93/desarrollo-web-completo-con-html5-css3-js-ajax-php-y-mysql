//Creando una funcion

/****************function declaration: FUNCIONA si se llama antes de declararla*********/
function saludar(){//todo que este adentro es el cuerpo de la funcion
	console.log('Hola');
}
//Una funcion se puede llamar varias veces. Puede uno reutilizar codigo 
saludar();//llamando a la funcion

saludo('Jesus', 'Desarrollador web');
saludo('Alejandra');
saludo('Laura', 'traductor');
function saludo(nombre, trabajo = 'No sabemos'){
	console.log('Hola ' + nombre + ' tu trabajo es ' + trabajo);
}

/***************************function expression**********************/
//NOTA: si se llama a la funcion antes no va a funcionar
const suma = function(){
	console.log(1 + 2);
}
suma();

const sumar = function(a = 0, b = 0){
	console.log(a + b);
}
sumar();
sumar(8)
sumar(10,20);
sumar(30,50);
sumar(100,200);

/**********************************IIFE*****************************************/
//funcion que se invoca mandandose a llamar ella misma
(function (tecnologia){
	console.log('AQUI ESTOY!! aprendiendo' + tecnologia);
})('Javascript');