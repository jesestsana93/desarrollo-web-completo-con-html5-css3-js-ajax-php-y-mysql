//object literal enhacement
//sirve para unir objetos
const banda = 'Metallica',
	genero = 'Heavy Metal',
	canciones = ['Master of puppets', 'Seck & destroy', 'Enter Sandman'];

//forma anterior de hacerlo
const metallica = {
	banda: banda,
	genero: genero,
	canciones: canciones
}

console.log(metallica);

//forma nueva de hacerlo. si se llaman igual se escriben una sola vez
const metallica = {	banda, genero, canciones}