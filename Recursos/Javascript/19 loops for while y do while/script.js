//for loop
console.log('******************FOR LOOP****************');
for(let i = 0; i < 10; i++){
	console.log(`Numero: ${i}`)
}
console.log('**********CARRITO DE COMPRAS*************');
const carrito = ['Mango', 'Fresa', 'Sandia', 'Platano', 'Manzana', 'Melon', 'Papaya'];

for(let k = 0; k < carrito.length; k++){
	console.log(`Fruta en el carrito: ${carrito[k]}`);
}

//while loop
console.log('**********WHILE LOOP*********');
let i = 0;
while(i<10){
	console.log(`Numero: ${i}`);
	i++;
}

/*while(i<carrito.legth){
	console.log(`Producto en el carrito: ${carrito[i]}`);
	i++;
}*/

let j = 11;
console.log('Do while loop con j=11');
do{
	console.log(j);
	j++;
}while(j<10);