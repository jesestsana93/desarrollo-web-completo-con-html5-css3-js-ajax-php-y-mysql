let aprendiendo = 'Aprendiendo', 
	tecnologia = 'Javascript';

//Concatenacion de cadenas
console.log(aprendiendo + ' ' + tecnologia);
//Forma nueva
console.log(`${aprendiendo} ${tecnologia}`);

//Funciones para strings o cadenas de texto
let banda = 'Metallica', 
	cancion = 'Enter Sandman';

let nombre;
nombre = banda + ": " + cancion;

//length nos dice cuantas letras tiene una variable
console.log(banda.length);

//concat
nombre = nombre.concat(" ", "y es genial");

//todas mayusculas
nombre = nombre.toUpperCase();
console.log(nombre);

//todas minusculas
nombre = nombre.toLowerCase();
console.log(nombre);

//split va a dividir una cadena de texto
let actividad = 'Estoy aprendiendo JavaScript con el curso';
nombre = actividad.split(' ');
console.log(nombre);

let intereses = 'Leer, caminar, escuchar musica, escribir, aprender a programar';
nombre = intereses.split(", ");
console.log(nombre);

//revisa que algo exista
nombre = actividad.includes('JavaScript');
console.log(nombre);

//reemplazar un valor
nombre = actividad.replace('JavaScript', 'JS');
console.log(nombre);

let master = 'Master',
	puppets = 'Of Puppets';

console.log(master.repeat(3));
console.log(puppets);