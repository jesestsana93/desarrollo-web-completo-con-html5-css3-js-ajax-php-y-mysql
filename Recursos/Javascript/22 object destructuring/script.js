//destructuring
const cliente = {
	nombre: 'Alejandra',
	tipo: 'Premium',
	datos: {
		ubicacion: {
			ciudad: 'Mexico',
			pais: 'Mexico'
		},
		cuenta: {
			desde: '10-12-2012',
			saldo: 4000
		}
	}
}

console.log(cliente);

//crear la variable
//forma anterior
const nombreCliente = cliente.nombre;
console.log(nombreCliente);

const nombreCliente2 = cliente.nombre,
	tipoCliente = cliente.tipo,
	ubicacionCliente = cliente.datos.ubicacion;
console.log(ubicacionCliente);

console.log('**************destructuring***************')
//destructuring de nueva version para extraer un objeto
let {nombre} = cliente;
console.log(cliente);
console.log(nombre);

let {nombre2,tipo} = cliente;
console.log(tipo);

let{datos: {ubicacion}} = cliente;
console.log(ubicacion);

let{datos: {cuenta: {saldo}}} = cliente;
console.log(saldo);
