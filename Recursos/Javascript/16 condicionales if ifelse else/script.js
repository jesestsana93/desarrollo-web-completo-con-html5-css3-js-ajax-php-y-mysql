const puntaje = 1000;

if(puntaje == '1000'){
	console.log('Si es igual');
} 

if(puntaje === '1000'){//operador estricto que revisa que el tipo de dato sea el mismo
	console.log('Si es igual');
} else{
	console.log('No no es igual');
}

if(puntaje !== '1000'){
	console.log('No es igual');
}else{
	console.log('Si es igual');
}

const logueado = true;

if(logueado){
	console.log('Si estas logueado');
}else{
	console.log('No estas logueado');
}

const edad = 26;
if(edad >=20){
	console.log('Si eres mayor de edad');
}else{
	console.log('No eres mayor de edad');
}

let dinero = 500;
let totalCarrito = 300;

if(dinero > totalCarrito){
	console.log('Pago correcto');
}else{
	console.log('Fondos insuficientes');
}

let tarjeta = true;
if(dinero === totalCarrito){
	console.log('Pago correcto');
}else if(tarjeta){
	console.log('Pagaste con tarjeta');
}else{
	console.log('Fondos insuficientes');
}