//function expression
const sumar = function(a = 0, b = 0){
	return a + b;
}
//arrow functions
//const suma = (a = 0, b = 0) => a + b;

const multiplicar = function(a){
	return a * 5;
}
/*con arrow function: 
const multiplicar = a => a * 5;
*/

let total;

let resultadoSuma = sumar(10+20);
total = multiplicar(resultadoSuma);
console.log(total);

let resultadoSuma2 = sumar(3, 5);
total = multiplicar(resultadoSuma2);
console.log(total);

/*let viajando = function(destino){
	return 'Viajando a la ciudad de ' + destino;
}*/
//con arrow function
let viajando = destino => 'Viajando a la ciudad de ' + destino;

let viaje;
viaje = viajando('Londres');
viaje = viajando('Paris');
console.log(viaje);