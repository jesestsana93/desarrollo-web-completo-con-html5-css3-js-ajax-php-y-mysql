//En javascript existen 3 formas de crear variables

var x;
var apellido, correo;

//Convencion de separacion para nombrar una variable 
var nombreCliente = 'Laura'; //camelCase
var nombre_cliente = 'Jesus'; //underscore
var NombreCliente = 'Susana'; //Pascal case

console.log(nombre);

var nombre = 'Jesus';

console.log(nombre);

nombre = true;

x=false;

var edad;
edad=26;

console.log(nombre);

/*FORMA 2*/
let nombre2;
nombre2= 'karen';

console.log(nombre2);

/*forma 3*/
const cliente='No puede reescribirse y siempre tiene que tener un valor';
console.log(cliente);