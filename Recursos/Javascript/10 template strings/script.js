//Template strings
const nombre = 'Jesus',
	trabajo = 'Desarrollador web';

console.log("Nombre: " + nombre + ', Trabajo: ' + trabajo);//version antigua sin template strings;
console.log(`Nombre: ${nombre}, Trabajo: ${trabajo}`);//con template strings

//querySelector permite seleccionar un elemento del html e inyectarle codigo
const contenedorApp = document.querySelector('#mensaje');
let html = '<ul>' + 
				'<li>Nombre: ' + nombre + '</li>' + 
				'<li>Trabajo: ' + trabajo + '</li>' + 
			'</ul>';

//con template strings no es necesario poner el + que se podria olvidar y hacer que no funcione
let html2 = `<ul>
				<li>Nombre: ${nombre}</li>
				<li>Trabajo: ${trabajo}</li>
			</ul>`;

contenedorApp.innerHTML = html2;