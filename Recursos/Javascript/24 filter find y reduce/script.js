const personas = [
	{nombre:'Jesus', edad:26},
	{nombre:'Pablo', edad:50},
	{nombre:'Alejandra', edad:23, aprendiendo: 'JavaScript'},
	{nombre:'Karen', edad:24},
	{nombre:'Miguel', edad:33}
];
console.table(personas);

//Obtener las personas mayores de 25 años con filter
console.log('******Personas mayores a 25 años*********')
const mayores = personas.filter(persona => persona.edad > 25);

console.log(mayores);


//Extraer informacion de Alejandra
const alejandra = personas.find(persona=> {
	return persona.nombre === 'Alejandra'
});
console.log(alejandra);	
let {aprendiendo} = alejandra;
console.log('Aprendiendo ' + aprendiendo);

//reduce
let total = personas.reduce((edadTotal, persona) =>{
	return edadTotal + persona.edad;
}, 0);
console.log('Suma de las edades de las personas: ' + total);

console.log('Promedio de edad: ' + total / personas.length);