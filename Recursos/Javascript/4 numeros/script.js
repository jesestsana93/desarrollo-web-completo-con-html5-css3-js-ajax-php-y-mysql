const numero1 = 30,
	numero2 = "20",
	numero3 = 20.20,
	numero4 = -3;
	numero5 = 20;

console.log(numero1);
console.log(numero2);

let resultado;
//operaciones
resultado = numero1 + numero5;
console.log(resultado);

resultado = numero1 - numero5;
console.log(resultado);

resultado = numero1 * numero5;
console.log(resultado);

resultado = numero1 / numero5;
console.log(resultado);

//clase Math
resultado = Math.round(2.5);//redondea un numero

resultado = Math.floor(2.5);//redondea hacia abajo
resultado = Math.ceil(2.1);//redondea hacia arriba

resultado = Math.sqrt(144); //raiz cuadrada
resultado = Math.abs(numero4);//devuelve el valor absoluto quitandole el negativ
resultado = Math.pow(8,3); //potencia de un numero

resultado = Math.min(10,2,4,60,5); //dice cual es el numero minimo
resultado = Math.max(10,2,4,60,5); //dice cual es el numero maximo
 