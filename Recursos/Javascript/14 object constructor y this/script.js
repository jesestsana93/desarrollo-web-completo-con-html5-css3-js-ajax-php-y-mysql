//object literal (es mas estatico y mas usado)
const persona = {
	nombre: 'Jesus',
	edad: 26,
	anioNacimiento: function(){
		return new Date().getFullYear() - this.edad;
	}
}
console.log(persona);
console.log(persona.anioNacimiento());

//Object constructor (new) FORMA ANTERIOR DE CREAR CLASES
function Tarea(nombre, urgencia){
	//para no perder la referencia y sepa a que instancia pertenece se usa this
	this.nombre = nombre; 
	this.urgencia = urgencia;
}

//crear nuevas tareas
cons tarea1 = new Tarea('Aprender javascript', 'Urgente');
console.log(tarea1);

cons tarea2 = new Tarea('Pasear al perro', 'media');
console.log(tarea2);

cons tarea2 = new Tarea('Bailar', 'urgente');
console.log(tarea3);

//Javascript ya en las nuevas versiones soporta clases
class Tarea{
	constructor(nombre, urgencia){
		this.nombre = nombre; 
		this.urgencia = urgencia;
	}
}