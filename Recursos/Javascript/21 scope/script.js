//Scope: visibilidad de una variable. No aplica con var. Solo con let y const
let musica = 'rock'; //global

if(musica){
	let musica = 'Grunge';//local
	console.log('Dentro del if ' + musica);
}
console.log('Fuera del if ' + musica);