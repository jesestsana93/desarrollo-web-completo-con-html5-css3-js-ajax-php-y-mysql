//En un objeto se usan las llaves, a diferencia del arreglo que va con corchetes
const persona = {
	nombre: 'Jesus',
	apellido: 'Sanchez',
	edad: 80,
	trabajo:true,
	musica: ['Salsa', 'Bachata', 'Cumbia' , 'reggaeton', 'electronica' , 'pop'],
	hogar:{
		ciudad: 'Guadalajara',
		pais: 'Mexico'
	}
}

console.log(persona);
console.log(persona.edad);//se accede a la posicion con un punto
console.log(persona.hogar.pais);
