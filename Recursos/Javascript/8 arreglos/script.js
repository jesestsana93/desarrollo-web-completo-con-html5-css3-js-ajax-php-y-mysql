//Crear un arreglo
const numeros = [10, 20, 30, 40, 50];
console.log(numeros);
console.table(numeros);

const meses = new Array('enero', 'febrero', 'marzo');
console.log(meses[1]);

meses.push('Abril');//agrego al fin del arreglo
console.log(meses);

meses.push('Mayo');
console.log(meses);

meses.unshift('Mes 0'); //agrego al inicio del arreglo
console.log(meses);

meses.pop();//elimina del final del arreglo
console.log(meses);

meses.shift();//elimina al inicio del arreglo
console.log(meses);

//eliminar de un rango del arreglo
meses.splice(0,1);
console.log(meses);

meses.reverse();//cambia el orden
console.log(meses);

let frutas = ['Platano', 'Manzana', 'Fresa', 'Naranja'];//ordenar un arreglo

frutas.sort();//ordenar un arreglo
console.log(frutas);

const arreglo = ['Hola', 10, true, "si", null, undefined];
console.log(arreglo.length);

console.log(Array.isArray(arreglo));