<?php include 'includes/header.php';

//variables
$nombre = "Juan";

//reasignacion de variable
$nombre = "Juan 2";

echo $nombre;
var_dump($nombre);

//Constantes
define('constante', "Este es el valor de la constante");
echo constante;

const constante2 = "Hola 2";
echo constante2;

$nombreCliente = "Pedro";
$nombre_cliente = "Pedro";

include 'includes/footer.php';