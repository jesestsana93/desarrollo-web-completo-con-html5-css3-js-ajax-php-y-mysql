<?php

function usuario_autenticado() {
    if(!revisar_usuario() ){
        header('Location:login.php');//redireccionamos
        exit();
    }
}
function revisar_usuario() {
    return isset($_SESSION['nombre']);
}
session_start();//arrancamos una sesion 
usuario_autenticado();//revisa que exista y si no esta se va a login para que inicie sesion
